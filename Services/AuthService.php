<?php

namespace Modules\User\Services;

use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;
use Modules\User\Events\UserRegisteredEvent;
use Modules\User\Exceptions\EmailAlreadyVerifiedException;
use Modules\User\Exceptions\OTPDoNotMatchException;
use Modules\User\Exceptions\WrongCredentialsException;
use Modules\User\Notifications\SendEmailVerificationNotification;
use Modules\User\Notifications\SendForgotPasswordNotification;

class AuthService
{
    public function register(array $data)
    {
        $user = User::create([
            ...$data,
            'otp' => $this->generateOTP()
        ]);

        UserRegisteredEvent::dispatch($user);

        return [
            'token' => $user->createToken($data['email'])->plainTextToken,
            'user' => $user
        ];
    }

    /**
     * Login Action
     *
     * @param array $data
     * @return array
     * @throws WrongCredentialsException
     */
    public function login(array $data): array
    {
        if (!$user = User::where('email', $data['email'])->first()) {
            throw new WrongCredentialsException();
        }

        if (!Hash::check($data['password'], $user->password)) {
            throw new WrongCredentialsException();
        }

        return [
            'token' => $user->createToken($user->email)->plainTextToken,
            'user' => $user,
        ];
    }

    public function logout(): void
    {
        auth()->user()->currentAccessToken()->delete();
    }

    private function generateOTP(): int
    {
        do {
            $otp = rand(User::OTP_LENGTH[0], User::OTP_LENGTH[1]);
        } while (User::where('otp', $otp)->exists());

        return $otp;
    }

    public function verifyEmail(array $data)
    {
        $user = User::where('otp', $data['otp'])->first();

        $user->update([
            'email_verified_at' => now(),
            'otp' => null
        ]);
    }

    /**
     * @return void
     * @throws EmailAlreadyVerifiedException
     */
    public function resendEmailVerification(): void
    {
        if (auth()->user()->email_verified_at) {
            throw new EmailAlreadyVerifiedException();
        }

        auth()->user()->update([
            'otp' => $this->generateOTP()
        ]);

        auth()->user()->notify(new SendEmailVerificationNotification());
    }

    public function sendForgotPasswordEmail(): void
    {
        auth()->user()->update([
            'otp' => $this->generateOTP()
        ]);

        auth()->user()->notify(new SendForgotPasswordNotification());
    }

    /**
     * @param array $data
     * @return void
     * @throws OTPDoNotMatchException
     */
    public function changeForgottenPassword(array $data): void
    {
        if (auth()->user()->otp !== $data['otp'])
        {
            throw new OTPDoNotMatchException();
        }

        auth()->user()->update([
            'password' => $data['password'],
            'otp' => null
        ]);
    }
}
