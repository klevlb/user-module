<?php

namespace Modules\User\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Events\UserRegisteredEvent;
use Modules\User\Notifications\SendEmailVerificationNotification;

class SendEmailVerificationListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param UserRegisteredEvent $event
     * @return void
     */
    public function handle(UserRegisteredEvent $event)
    {
        $event->user->notify(new SendEmailVerificationNotification());
    }
}
