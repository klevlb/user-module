<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Model
{
    use HasFactory, HasApiTokens, Notifiable;

    const OTP_LENGTH = [1000, 9999];
    protected $fillable = [
        'name',
        'email',
        'password',
        'otp',
        'email_verified_at'
    ];

    protected $hidden = [
        'password'
    ];

    protected $casts = [
        'password' => 'hashed'
    ];

//    protected static function newFactory()
//    {
//        return \Modules\User\Database\factories\UserFactory::new();
//    }
}
