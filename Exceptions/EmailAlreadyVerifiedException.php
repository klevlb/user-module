<?php

namespace Modules\User\Exceptions;

use Exception;

class EmailAlreadyVerifiedException extends Exception
{
    public function render()
    {
        return response()->json(["message" => "User's email already verified."], 403);
    }
}
