<?php

namespace Modules\User\Exceptions;

use Exception;

class OTPDoNotMatchException extends Exception
{
    public function render()
    {
        return response()->json(["message" => "OTP does not match"], 403);
    }
}
