<?php

namespace Modules\User\Exceptions;

use Exception;

class WrongCredentialsException extends Exception
{
    public function render()
    {
        return response()->json(["message" => "Wrong Email or Password"], 401);
    }
}
