<?php

namespace Modules\User\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\User\Events\UserRegisteredEvent;
use Modules\User\Listeners\SendEmailVerificationListener;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        UserRegisteredEvent::class => [
            SendEmailVerificationListener::class
        ]
    ];
}
