<?php

use Illuminate\Support\Facades\Route;
use Modules\User\Http\Controllers\AuthController;

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('verify-email', [AuthController::class, 'verifyEmail']);
    Route::post('resend-email-verification', [AuthController::class, 'resendEmailVerification']);
    Route::post('send-forgot-password-email', [AuthController::class, 'sendForgotPasswordEmail']);
    Route::post('check-forgot-password-otp', [AuthController::class, 'checkForgottenPasswordOTP']);
    Route::post('change-forgotten-password', [AuthController::class, 'changeForgottenPassword']);
});

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
