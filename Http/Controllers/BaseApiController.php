<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseApiController extends Controller
{
    public function success($data = [], $message = "", $code = Response::HTTP_OK)
    {
        return response()->json([
            "data" => $data,
            "message" => $message,
            "success" => true
        ], $code);
    }

    public function error($message = "", $code = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        return response()->json([
            "message" => $message,
            "success" => false
        ], $code);
    }
}
