<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Modules\User\Exceptions\EmailAlreadyVerifiedException;
use Modules\User\Exceptions\OTPDoNotMatchException;
use Modules\User\Exceptions\WrongCredentialsException;
use Modules\User\Http\Requests\ChangePasswordRequest;
use Modules\User\Http\Requests\LoginRequest;
use Modules\User\Http\Requests\RegisterRequest;
use Modules\User\Http\Requests\CheckOTPRequest;
use Modules\User\Services\AuthService;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends BaseApiController
{
    public function __construct(protected AuthService $service)
    {
    }

    /**
     * Register A New User
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        return $this->success(
            data: $this->service->register($request->validated()),
            code: Response::HTTP_CREATED
        );
    }

    /**
     * Login User
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws WrongCredentialsException
     */
    public function login(LoginRequest $request)
    {
        return $this->success(
            data: $this->service->login($request->validated()),
            code: Response::HTTP_ACCEPTED
        );
    }

    /**
     * Logout User
     * @return JsonResponse
     */
    public function logout()
    {
        $this->service->logout();

        return $this->success(
            code: Response::HTTP_NO_CONTENT
        );
    }

    /**
     * Verify User's Email
     * @param CheckOTPRequest $request
     * @return JsonResponse
     */
    public function verifyEmail(CheckOTPRequest $request)
    {
        $this->service->verifyEmail($request->validated());

        return $this->success(
            code: Response::HTTP_NO_CONTENT
        );
    }

    /**
     * Resend User Email Verification
     * @return JsonResponse
     * @throws EmailAlreadyVerifiedException
     */
    public function resendEmailVerification()
    {
        $this->service->resendEmailVerification();

        return $this->success(
            code: Response::HTTP_NO_CONTENT
        );
    }

    /**
     * Send User forgot password email
     * @return JsonResponse
     */
    public function sendForgotPasswordEmail()
    {
        $this->service->sendForgotPasswordEmail();

        return $this->success(
            code: Response::HTTP_NO_CONTENT
        );
    }

    /**
     * Change User's password
     * @throws OTPDoNotMatchException
     */
    public function changeForgottenPassword(ChangePasswordRequest $request)
    {
        $this->service->changeForgottenPassword($request->validated());

        return $this->success(
            code: Response::HTTP_NO_CONTENT
        );
    }

    /**
     * Check if OTP valid for forgot password
     * @param CheckOTPRequest $request
     * @return JsonResponse
     */
    public function checkForgottenPasswordOTP(CheckOTPRequest $request)
    {
        $request->validated();

        return $this->success(
            code: Response::HTTP_NO_CONTENT
        );
    }
}
